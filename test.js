const assert = require('assert')

describe('First tests', () => {
	describe('42 == 42', () => {
		it('should be false', () => {
			assert.notEqual(42,24)
		})
	})
})

describe('Second tests', () => {
	describe('0 == 0', () => {
		it('should be true', () => {
			assert.equal(0,0)
		})
	})
})
