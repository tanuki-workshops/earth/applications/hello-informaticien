FROM node:13.12-slim
# Create and change to the app directory.
WORKDIR /usr/src/app
COPY package*.json ./
# Install production dependencies.
RUN npm install --only=production
# Copy local code to the container image.
COPY . ./
CMD [ "npm", "start" ]
